
# 表達習慣介紹

我們習慣用前置 `$ some-command` 代表這個是小黑窗的指令,
以這個例子來說指令式 some-command.


# 從空白資料夾中建立倉庫

首先建立一個空白資料夾`$ mkdir mydir`
然後切換到這個資料夾中`$ cd mydir`

`$ git init`可以在現在的資料夾內建立一個`.git`隱藏資料夾.
他存放著專案中有關版本控制的所有資訊.
如果哪天不想用git來做版本控制了, 只要直接把`.git`刪除即可.
這個`.git`可以看成是一個倉庫(repository),
所有與`.git`同一層資料夾的檔案都會被納入版本控制.

# git config

你需要設定你的名字和email, 這個是必須的.

~~~shell
$ git config --global user.name "ranlempow"
$ git config --global user.email ranlempow@users.noreply.github.com
~~~


# 名詞解釋


- work tree
    工作目錄, .git資料夾外的整個目錄, 實際在檔案系統中的目錄.
- stage
    暫存區, 一個虛擬的工作區, 介於歷史與工作目錄之間
- commit
    當作名詞時代表一個歷史中的版本,
    作為動詞時則是checkin的意思,
    以一串十六進位的號碼來當作名字.
- checkin
  利用stage製作一個commit, 加入歷史中.
- checkout
  從過去歷史簽出一個版本到工作目錄.


# 初次見面

`$ git status`用來查看檔案的狀態.
現在會顯示
~~~
nothing to commit (create/copy files and use "git add" to track)
~~~
因為我們還沒有任何檔案.

我們來製作一個簡單的檔案.
`$ echo "hello world." > fileA.txt`
或是打開文字編輯器, 輸入一行`hello world.`, 在存擋到工作目錄中, 檔名為`fileA.text`.

然後再次`$ git status`.
~~~
Untracked files:
  (use "git add <file>..." to include in what will be committed)

        fileA.txt
no changes added to commit (use "git add" and/or "git commit -a")
~~~

這個意思代表工作目錄中有一個新的檔案, 但是這個檔案還沒被追蹤.

~~~shell
$ git add fileA.txt
~~~
告訴git我們把`fileA.txt`加入到stage中, 並且追蹤這個檔案.

~~~
Changes to be committed:

        new file:   fileA.txt
~~~

現在stage中有新東西了, 我們可以簽入一個新版本了
~~~shell
$ git commit -m "My first Commit!"
~~~

會顯示這個訊息代表製作commit成功, 為了避免繁瑣, 本教學之後不會再提示這個訊息.
~~~
[master (root-commit) f37f943] I can use git!
 1 file changed, 1 insertion(+)
 create mode 100644 fileA.txt
~~~

現在我們來看一下歷史, 使用`$ git log`
~~~
commit f37f943f570d3fae6d6ec5361539251d0eac422b (HEAD -> master)
Author: ranlempow <ranlempow@users.noreply.github.com>
Date:   Mon Mar 25 20:50:58 2019 +0800

    My first Commit!
~~~
第一行是這個commit的十六進位代號, 括號是分支, 我們下一章才會提到分支,
第二行是簽入者的名字,
第三行是簽入的日期,
第五行之後是你之前用`-m`輸入的版本訊息.


完整的歷史訊息太長了, 我們用`$ git log --oneline`來簡化
~~~
f37f943 (HEAD) I can use gitgit log!
~~~
一行一個commit, 依序是十六進位代號縮寫, 分支, 版本訊息.


# 更多的新版本

讓我們來加入更多的檔案吧.
如果不熟悉小黑窗的同學, 也可以使用文字編輯器做以下這些檔案操作喔.

首先在之前的`fileA.txt`的結尾加入新的一行
`$ echo "add the line." >> fileA.txt`

增加一個叫做`fileB.txt`的新檔案
`$ echo "more files" > fileB.txt`


`$ git status`觀察一下現在倉庫的狀態
~~~
Changes not staged for commit:

        modified:   fileA.txt

Untracked files:

        fileB.txt
no changes added to commit (use "git add" and/or "git commit -a")
~~~
跟上一次一樣有一個新的未追蹤檔案`fileB.txt`.

而`fileA.txt`因為已經有追蹤了,`$ git status`告訴我們`fileA.txt`被更改了但是還沒進入stage.

`$ git add fileA.txt`把更改過的`fileA.txt`加入stage.

`$ git status`再次執行
~~~
Changes to be committed:

        modified:   fileA.txt

Untracked files:

        fileB.txt
~~~
你會發現`fileA.txt`變成綠色字體, 那代表他已經進入stage, 可以被簽入.



`git add fileB.txt`把新的`fileB.txt`加入stage.

`$ git status`再次執行
~~~
Changes to be committed:

        modified:   fileA.txt
        new file:   fileB.txt
~~~
現在`fileA.txt`, `fileB.txt`都進入stage了.


`$ git commit -m "Add more files!"` 和之前一樣簽入, 製作新的版本.

`$ git status`這次我們仔細看一下, 簽入成功之後stage被清空了, 工作目錄也乾淨了.
~~~
nothing to commit, working tree clean
~~~

# 刪除檔案好了

如果不熟悉小黑窗的同學, 可以使用檔案總管操作也可以喔.


`$ rm fileB.txt`把`fileB.txt`刪除.

`$ git status`查看狀態.
~~~
Changes not staged for commit:

        deleted:    fileB.txt

no changes added to commit (use "git add" and/or "git commit -a")
~~~
git發現被追蹤的`fileB.txt`不見了.


`$ git add fileB.txt`我們一樣要把這個變化加入stage, 之後才可以將這個變化簽入到歷史中.
~~~
Changes to be committed:

        deleted:    fileB.txt
~~~
很好, 刪除`fileB.txt`這件事情已經加入stage中, 可以簽入了.


`$ git commit -m "Delete fileB.txt!"` 跟之前一樣簽入指令.

`$ git log --oneline`看一下現在歷史
~~~
8f813db (HEAD -> master) Delete fileB.txt!
4f8c5bf Add more files!
f37f943 I can use gitgit log!
~~~
一行一個版本, 頂端是最新的版本, 尾端是最舊的.


# 時光機？

我們可以回到過去的版本, 也就是將之前的版本簽出到工作目錄之中.
這個動作叫做簽出(checkout).

`$ git log --oneline`看一下歷史
~~~
8f813db (HEAD -> master) Delete fileB.txt!
4f8c5bf Add more files!
f37f943 I can use gitgit log!
~~~
假如我們要簽出前一個版本, 也就是第二行的版本.

`$ git checkout 4f8c5bf`這個指令會把`4f8c5bf`這個版本簽出.

注意, 這個`4f8c5bf`版本名稱要換成自己電腦`$ git log`所顯示的的.

`$ git log --oneline`
~~~
4f8c5bf (HEAD) Add more files!
f37f943 I can use gitgit log!
~~~
有沒注意到歷史變短了.

如果我們依然想看完整了歷史呢.

`$ git log --oneline master`後面多了`master`這個分支的名稱,
於是我們可以看到`master`分支的全部歷史.
~~~
8f813db (master) Delete fileB.txt!
4f8c5bf (HEAD) Add more files!
f37f943 I can use gitgit log!
~~~
第二的行HEAD代表我們現在所在的位置.
這件事情很重要.


`$ ls` 看一工作目錄中, 果然之前被刪除的`fileB.txt`回來了.
~~~
fileA.txt  fileB.txt
~~~


`$ git checkout master`現在讓我們回到主線(master)

`$ git log --oneline`
~~~
8f813db (HEAD -> master) Delete fileB.txt!
4f8c5bf Add more files!
f37f943 I can use gitgit log!
~~~
回到最新的版本`8f813db`, 一切恢復正常.


`$ ls` 工作目錄中`fileB.txt`又不見了, 這跟我們預料一樣.
~~~
fileA.txt
~~~


# TODO

TODO: 舊版的bash對驚嘆號有障礙

